// Отправка заявки 
$(document).ready(function() {
	$('#form').submit(function() { // проверка на пустоту заполненных полей. Атрибут html5 — required не подходит (не поддерживается Safari)
		if (document.form.name.value == '' || document.form.phone.value == '' ) {
			valid = false;
			return valid;
		}
		$.ajax({
			type: "POST",
			url: "mail.php",
			data: $(this).serialize()
		}).done(function() {
			$('.js-overlay-thank-you').fadeIn();
			$(this).find('input').val('');
			$('.overlay_popup').fadeOut(); 
			$('.form').fadeOut();
			$('#form').trigger('reset');
		});
		return false;
	});

    // Маска ввода номера телефона (плагин maskedinput)
    $(function($){
        $('[name="phone"]').mask("+7(999) 999-9999");
    });

});

// Закрыть попап «спасибо»
$('.js-close-thank-you').click(function() { // по клику на крестик
	$('.js-overlay-thank-you').fadeOut();
});

$('.close').click(function() { // по клику на крестик
    $('.overlay_popup, .popup').fadeOut();
	$('.form, .popup').fadeOut();
});

$(document).mouseup(function (e) { // по клику вне попапа
    var popup = $('.popup');
    if (e.target!=popup[0]&&popup.has(e.target).length === 0){
        $('.js-overlay-thank-you').fadeOut();
    }
});


$('.show_popup').click(function() { // Вызываем функцию по нажатию на кнопку
    $('.form').fadeIn(); // Открываем окно
    $('.overlay_popup').fadeIn(); // Открываем блок заднего фона
  })

  $('.overlay_popup').click(function() { // Обрабатываем клик по заднему фону
    $('.overlay_popup, .popup').fadeOut(); // Скрываем затемнённый задний фон и основное всплывающее окно
    $('.form, .popup').fadeOut();
  })

