import { TimelineMax } from "gsap/all";

let tl = new TimelineMax();

document.addEventListener("DOMContentLoaded", ready);
function ready() {
  asyncPlayBGVideo ()
  let width = window.innerWidth;

  if(width<420){
    let sckills = document.querySelector('.hero_skils__wrapper');
    sckills.scrollLeft=90
  }
  $(window).scroll(function() {
    var scroll = $(window).scrollTop();
    if (scroll >= 100) {
      $('.header').addClass("header--fixed");
    } else{
      $('.header').removeClass("header--fixed");
    }
  });
}

function asyncPlayBGVideo(){
  let bgvideo =document.querySelector('#bgvideo')
  var playPromise = bgvideo.play();

  if (playPromise !== undefined) {
    playPromise.then(_ => {
      bgvideo.play();
    })
    .catch(error => {
      console.log(error)
    });
  }
}

let btnMenu = document.querySelector('.mobile_nav_logo');
let menuBG = document.querySelector('.menu_bg');
let menuContainer = document.querySelector('.mobile_menu')

btnMenu.addEventListener("click", toggleMenu);
let menuIsOpen = false;

function toggleMenu() {
  if (menuIsOpen) {
    menuIsOpen = false;
    console.log('tag', menuIsOpen)
    tl.to(menuContainer, 0, {
      height: 100 + "vh",
      ease: "power2.out"
    });
    tl.to(menuBG, 0, {
      height: 100 + "vh",
      ease: "power2.out"
    });
  } else {
    menuIsOpen = true;
    console.log('tag', menuIsOpen)
    tl.to(menuContainer, 0, {
      height: 0 + 'vh',
      ease: "power2.out"
    });
  }
}